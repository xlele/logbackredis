package com.elkdemo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * Created by IBM on 2016/5/6.
 */
@Component
public class LogDemoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(LogDemoService.class);

    public void generateLog(){
        LOGGER.info("hello:{}", UUID.randomUUID().toString());
    }
    public void jsonLog(){
        LOGGER.info("{\"name\":\"{}\"}", UUID.randomUUID().toString());
    }
}
